<a class="inline-link" name="top"><a>
inline links work by way of using embedded html.

You can use inline linking in markdown with inline html.

Here is an example:
`<a class="inline-link" name="linkname"><a>`

You can navigate using these inline links by putting a `#` after the html file.
```
http://edge226.gitlab.io/testwebsite/linkstuff/index.html#top
http://edge226.gitlab.io/testwebsite/linkstuff/index.html#windows
http://edge226.gitlab.io/testwebsite/linkstuff/index.html#linux
http://edge226.gitlab.io/testwebsite/linkstuff/index.html#osx
```

[Top](index.html#top)
[Windows](index.html#windows)
[Linux](index.html#linux)
[Mac](index.html#osx)

ghmd-ccs makes any file named README.md into index.html

This is why I know this file will be the one people see.

But we can take a look at [linkstuff](linkstuff.html)
<a class="inline-link" name="linkstuff"><a>

Here we have some random stuff.

This is just stuff...

We don't really need any of this.

I think I might copy and paste something I wrote before, Just to give some space. Maybe that thing I did last stream.

### Whats this docker thing?

Docker is a containerization technology that allows me to build a piece of software and have it operate on every operating system.

### Why is it relevent to me?

It allows you to do development and testing of software no matter what operating system you're using.

## Operating Systems instructions.

### Linux
<a class="inline-link" name="linux"><a>

These instructions are accurate as of the time of writing. You should Double check if newer instructions are available at the [docker website](https://www.docker.com/).

#### Ubuntu

Docker has official releases at this time for versions 16.10, 16.04 and 14.04 of Ubuntu.

The distribution specific instructions are located [here](https://store.docker.com/editions/community/docker-ce-server-ubuntu).

#### Debian

Docker has official releases at this time for version 7.7, 8.0 and Debian Stretch.

The distribution specific instructions are located [here](https://store.docker.com/editions/community/docker-ce-server-debian).

My personal testing is that over time software in Debian Jessie 8.0 was too old to even build newer versions of what was needed for a fully functional build system.

#### CentOS

Docker has official releases for the 64-bit version of CentOS 7.3.

The distribution specific instructions are located [here](https://store.docker.com/editions/community/docker-ce-server-centos).

#### Fedora

Docker has official releases for versions 24 and 25 of Fedora.

The distribution specific instructions are located [here](https://store.docker.com/editions/community/docker-ce-server-fedora).

### Windows
<a class="inline-link" name="windows"><a>

What you need to get docker working in Windows really depends on the version of Windows. The Windows subsystem for Linux changes how docker operates for Windows 10.

It is actually easier to install and test on Windows 10 than anything else at the moment.

#### Windows 10

Get docker for [Windows 10](https://store.docker.com/editions/community/docker-ce-desktop-windows) Professional/Enterprise 64-bit versions.

#### Other/Older Windows versions.

If what is described above does not work you will need the [docker toolbox](https://www.docker.com/products/docker-toolbox) which runs a small linux installation in Virtualbox. So it is likely required or will be installed during the process.

### Mac OS X
<a class="inline-link" name="osx"><a>

#### OS X El Capitan 10.11

This version of OS X is really easy to install and configure [docker](https://store.docker.com/editions/community/docker-ce-desktop-mac).

#### Older versions.

Older versions of docker operate through the [docker toolbox](https://www.docker.com/products/docker-toolbox). The docker toolbox uses virtualbox to operate.

#### What I've been told about Mac OS X.

Updates are free as long as the hardware supports it. You may want to do so if it allows you better docker support.

### How docker allows you to download and use automated software.

The gitlab container registry allows us to store and download images that have been preconfigured by the [Continuous integration](https://about.gitlab.com/features/gitlab-ci-cd/) system.

#### Our registries.
<a class="inline-link" name="registries"><a>

Image | location | Suggested command to test image | Other options
----- | -------- | ------------------------------- | -------------
archlinux host with dev-tools | `docker pull registry.gitlab.com/orbos/dev-tools` | `docker run --name=dev-tools --rm -it registry.gitlab.com/orbos/dev-tools` | `-e TERM="$TERM"`
core:tools temporary image. | `docker pull registry.gitlab.com/orbos/core:tools` | `docker run --name=coretools --rm -it registry.gitlab.com/orbos/core:tools` | `-e TERM="$TERM"`
core:dev-tools | `docker pull registry.gitlab.com/orbos/core:dev-tools` | `docker run --name=dev-tools --rm -it registry.gitlab.com/orbos/core:dev-tools` |`-e TERM="$TERM"`

#### Building your own images

By cloning our repository you gain access to the docker files we use to build Orbos. This means you can build your own versions, Test new changes that you make and all sorts of fun things!

Lets start with building a new host image of Arch Linux that can be used to build a temporary system capable of making a new system.

##### The host arch image.

```
git clone https://edge226@gitlab.com/orbos/dev-tools.git
cd dev-tools
cp -v Dockerfiles/Arch/Dockerfile ./
docker pull base/archlinux
docker build --no-cache --rm=true -t dev-tools .
docker run --name=dev-tools --rm -it dev-tools
```

##### The Core:dev-tools image.

```
git clone https://edge226@gitlab.com/orbos/dev-tools.git
cd dev-tools
cp -v Dockerfiles/core:dev-tools/Dockerfile ./
docker pull registry.gitlab.com/orbos/core:tools
docker build --no-cache --rm=true -t core:dev-tools .
docker run --name=dev-tools --rm -it core:dev-tools
```

On a Linux machine you will need to run the docker commands as root unless you add your user to the docker group. Adding your user to the docker group basically gives your user root access. This is probably insecure, Do it at your own risk.

#### Using volumes to import new data!

Using volumes is like mounting a directory into the docker image while you are running it.

This allows us to export data out of a docker image and also to save data and reuse it later inside of a docker image.

```
git clone https://edge226@gitlab.com/orbos/dev-tools.git
cd dev-tools
docker run -v "$PWD":/root/dev-tools --namedev-tools --rm -it registry.gitlab.com/orbos/core:dev-tools
touch /root/dev-tools/bin/testfile
## The next super ugly line generates a test script.
printf '#!%s\n\n%s\n%s${BASH_SOURCE[0]}%s\n%s\n' "/usr/bin/env bash" ". csl" "csl \"" "\"" "echo \"\$bin\"; echo \"\$lib\"; echo \"\$conf\"" > /root/dev-tools/bin/testfile
bash /root/dev-tools/bin/testfile
exit # exit the context of the docker container.
ls -l bin/testfile
```

#### what the heck does this CSL script do?

The csl library allows us to know whether sources are in the home directory or an installed location. If the launched location is in a home directory it will load libaries and configuration files from the git sources, Otherwise it will look for an installed location.

This allows us to have a stable and git testing version running side by side within the docker container.
